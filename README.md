How to use FreeFallDetection:

1-Initiate FreeFallDetection. It just needs your activity;

  FreeFallDetection freeFallDetection = new FreeFallDetection(this);
        
2- Run its detection:

 freeFallDetection.run();
 
 
Descriptions before start the project:

The project has been written by iman kazemini for practicing in the challenge code of Xbird company. In this document, the desired approach is discussed before the development. This document will compare with another document after finish the project.
Concerns: keep the library small and clean. 
The methodology of development: I’m going to use DDD methodology because of a lack of time. if I had more time, I could use TDD methodology to have more reliability for the application.
The methodology of design:  trying to break down user stories and add functionality to classes step by step.
Technical point of view:  I need to use two different services to support before and after the Orio(simple and job services). following the solid and clean code is necessary and for real-world-test App, I’m going to use MVP architecture. this will help me to write tests better. All the calculations must be efficient to decrease battery usages. Because of the type of challenge, designing a professional UI/UX won't be targeted.   I going to use design patterns if it is possible. The two shared preferences and SQLite seem suitable choices because I won't use an external library to implement them. however,  shared preference is not good for the huge amount of data.
Test Coverage: Around 10 percent. I going to have a 5 to 10 percent test coverage. 


Description after results:

This document is proposed to compare the result with before document about the approach. Some functionality and abilities added. For example a broadcast for re-launch services after termination. As I thought, the test coverage is not enough but roughly I have covered 5 percent coverage (base on the number of the functions and possible scenarios). I tried to use the best approach for detecting the freefall, these approaches are in these links:
  1-https://developer.android.com/reference/android/hardware/SensorEvent.html#values.
  2- https://www.hackster.io/RVLAD/free-fall-detection-using-3-axis-accelerometer-06383e.
  3- https://stackoverflow.com/questions/44518729/android-fall-detection  RARA answer.
The last one seems more accurate(by a test). However, I think Its needed more effort to find a more accurate way.
The clean code and solid used. By a checklist, I tried to make sure that.
the size of the library seems minimum. maybe with more effort, the size becomes less. 
Used design patterns: No special pattern addressed.


Future works
1- Boot complete: for run services after off the phone.
2- Add some functionality such as a clear database, a listener in Activity for listening to the events directly in Apps. The client wants to use this library when they are in the foreground.
3- More interface for working with database.
4- Alarm sound.
5- Help users by calling someone for emergency situations.



