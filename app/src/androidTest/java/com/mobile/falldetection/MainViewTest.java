package com.mobile.falldetection;

import androidx.test.rule.ActivityTestRule;
import com.mobile.falldetection.view.MainActivity;
import org.junit.Rule;
import org.junit.Test;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.greaterThan;

public class MainViewTest {

    @Rule
    public ActivityTestRule<MainActivity> activity2ActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void ensureCountOfList() {

        onView(withId(R.id.recycleView))
                .check(new RecyclerViewItemCountAssertion(greaterThan(0)));

    }


}
