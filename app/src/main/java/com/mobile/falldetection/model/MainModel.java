package com.mobile.falldetection.model;

import android.content.Context;

import com.mobile.falldetection.view.MainViewInterface;
import com.mobile.freefalldetection.FallModel;
import com.mobile.freefalldetection.FreeFallHistory;
import java.util.ArrayList;

public class MainModel implements ModelInterface {

    private MainViewInterface mainViewInterface;

    public MainModel(MainViewInterface mainViewInterface) {
        this.mainViewInterface = mainViewInterface;
    }

    @Override
    public ArrayList<FallModel> getEventList() {

        FreeFallHistory freeFallHistory = new FreeFallHistory();
        return freeFallHistory.getEventList(  (Context) mainViewInterface);
    }

}
