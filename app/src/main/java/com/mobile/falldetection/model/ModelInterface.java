package com.mobile.falldetection.model;

import com.mobile.freefalldetection.FallModel;

import java.util.ArrayList;

public interface ModelInterface {

    ArrayList<FallModel> getEventList();

}
