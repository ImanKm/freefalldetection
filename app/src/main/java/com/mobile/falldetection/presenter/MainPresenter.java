package com.mobile.falldetection.presenter;

import com.mobile.falldetection.model.MainModel;
import com.mobile.falldetection.view.MainViewInterface;


public class MainPresenter implements MainPresenterInterface {

    private MainViewInterface mainViewInterface;
    private MainModel mainModel;

    public MainPresenter(MainViewInterface mainViewInterface) {
        this.mainViewInterface = mainViewInterface;
        mainModel = new MainModel(mainViewInterface);
    }

    @Override
    public void runLogic() {
        mainViewInterface.initView();
        mainViewInterface.runDetection();
        mainViewInterface.showList(mainModel.getEventList());
    }

}
