package com.mobile.falldetection.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.mobile.falldetection.R;
import com.mobile.freefalldetection.FallModel;
import java.util.List;


public class EventAdapter extends RecyclerView.Adapter<EventAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private List<FallModel> arraylist;

    EventAdapter(Context context, List<FallModel> arraylist) {
        inflater = LayoutInflater.from(context);
        this.arraylist=arraylist;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.event_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.startTimeTxt.setText(arraylist.get(position).getStartTime());
        holder.durationTimeTxt.setText(arraylist.get(position).getDurationTime());
    }

    @Override
    public int getItemCount() {
        return arraylist.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView startTimeTxt;
        TextView durationTimeTxt;
         MyViewHolder(View itemView) {
            super(itemView);
            startTimeTxt = (TextView)itemView.findViewById(R.id.startTimeTxt);
            durationTimeTxt = (TextView)itemView.findViewById(R.id.durationTimeTxt);
        }
    }

}
