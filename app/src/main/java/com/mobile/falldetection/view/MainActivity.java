package com.mobile.falldetection.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import com.mobile.falldetection.R;
import com.mobile.falldetection.presenter.MainPresenter;
import com.mobile.freefalldetection.FallModel;
import com.mobile.freefalldetection.FreeFallDetection;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements MainViewInterface  {

    RecyclerView recyclerView;
    MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainPresenter = new MainPresenter(this);
        mainPresenter.runLogic();

    }

    @Override
    public void initView() {
        recyclerView = findViewById(R.id.recycleView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }

    @Override
    public void runDetection() {
        FreeFallDetection freeFallDetection = new FreeFallDetection(this);
        freeFallDetection.run();
    }

    @Override
    public void showList(ArrayList<FallModel> fallModels) {
        EventAdapter eventAdapter = new EventAdapter(this, fallModels);
        recyclerView.setAdapter(eventAdapter);
    }

}
