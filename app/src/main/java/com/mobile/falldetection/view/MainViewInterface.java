package com.mobile.falldetection.view;

import com.mobile.freefalldetection.FallModel;
import java.util.ArrayList;

public interface MainViewInterface {

    void initView();
    void runDetection();
    void showList(ArrayList<FallModel> fallModels);

}
