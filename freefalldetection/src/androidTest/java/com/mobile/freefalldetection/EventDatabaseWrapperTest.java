package com.mobile.freefalldetection;


import android.content.Context;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;
import org.junit.Test;
import org.junit.runner.RunWith;
import java.util.ArrayList;
import static org.junit.Assert.assertEquals;



@RunWith(AndroidJUnit4.class)
public class EventDatabaseWrapperTest {

    String[] inputAndExpected = new String[]{"StrinA","StrinB","",null};


    @Test
    public void CheckInsertAndListEvents() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();


        EventDataBaseWrapper eventDataBaseWrapper = new EventDataBaseWrapper(appContext);
        eventDataBaseWrapper.open();
        eventDataBaseWrapper.deleteAll();
        eventDataBaseWrapper.close(); // close to testing sequece and  creating synchronously connection

        eventDataBaseWrapper.open();

        for (String st: inputAndExpected) {
            eventDataBaseWrapper.insert(st,st);
        }

        eventDataBaseWrapper.close();


        FreeFallHistory freeFallHistory = new FreeFallHistory();
        ArrayList<FallModel> fallModels = freeFallHistory.getEventList(appContext);

        for(int i=0;i<fallModels.size();i++){
            assertEquals(fallModels.get(i).getDurationTime(), inputAndExpected[i]);
            assertEquals(fallModels.get(i).getStartTime(), inputAndExpected[i]);
        }

        eventDataBaseWrapper.open();
        eventDataBaseWrapper.deleteAll();
        eventDataBaseWrapper.close();

    }



}
