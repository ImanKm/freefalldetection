package com.mobile.freefalldetection;


class AccelerateCalculation {


    /*Calculate Gravity in each vector. The root of all three side of gravity and comparision
    * with threshold show whether device is falling or not
    * For next iteration: find more accurate threshold*/


    static boolean detect(float[] values){
        if(values==null || values.length<3){
            return false;
        }
        double loX = values[0];
        double loY = values[1];
        double loZ = values[2];

        double loAccelerationReader = Math.sqrt(Math.pow(loX, 2) + Math.pow(loY, 2) + Math.pow(loZ, 2));

        if (loAccelerationReader > 0.3d && loAccelerationReader < 0.5d) {
           return true;
        }
        return false;
    }


}
