package com.mobile.freefalldetection;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

class EventDataBaseWrapper {

    private DatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;

    EventDataBaseWrapper(Context ctx) {
        this.mCtx = ctx;
    }

    /*
    * The database must initiate and after that you must open it before any usage
    * Note: don't forget to close it. more than one opened thread may cause lock on DBManager
    * This database contains just one table and this table has three column
    * For next iterations: after each modification, you should increase the version of the database
    * For more information : https://developer.android.com/training/data-storage/sqlite
    * */

     void open() throws SQLException {
        mDbHelper = new DatabaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
    }

     void close() {
        if (mDb != null) {
            mDb.close();
        }
        if (mDbHelper != null) {
            mDbHelper.close();
        }
    }

    public static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DATABASE_CREATE1);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

            onCreate(db);
        }
    }

    private static final String DATABASE_NAME = "Movement";
    private static final String TABLE_NAME = "Falling";

    private static final int DATABASE_VERSION = 1;
    private final Context mCtx;

    //********************************************* Falling ***************************************************
    private static final String COL_ID = "_id";
    static final String COL_Fall_Start_TimeStamp = "StartTimeStamp";
    static final String COL_Fall_Duration = "FallDuration";


    private static final String DATABASE_CREATE1 =
            "CREATE TABLE if not exists " + TABLE_NAME + " ( " +
                    COL_ID + " INTEGER PRIMARY KEY autoincrement, " +
                    COL_Fall_Start_TimeStamp + " TEXT," +
                    COL_Fall_Duration + " TEXT);";


    //********************************************* methods ***************************************************

    void insert(String startTimeStamp,String duration) {
        ContentValues values = new ContentValues();
        values.put(COL_Fall_Start_TimeStamp, startTimeStamp);
        values.put(COL_Fall_Duration, duration);
        mDb.insert(TABLE_NAME, null, values);
    }

     Cursor fetchAll() {
        Cursor mCursor = mDb.query(TABLE_NAME, new String[]{COL_ID, COL_Fall_Start_TimeStamp, COL_Fall_Duration}, null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }


    void deleteAll() {
        mDb.delete(TABLE_NAME, null, null);
    }



}






