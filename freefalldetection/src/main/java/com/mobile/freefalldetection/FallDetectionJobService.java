package com.mobile.freefalldetection;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.RequiresApi;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
class FallDetectionJobService extends JobService {


    /*After Oreo android version, we have to use jobService instead of traditional services.
    * Notes:
    * 1- the traditional services won't work in background
    * 2- Job service is not available before M
    *  */


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public boolean onStartJob(JobParameters params) {
        Intent service = new Intent(getApplicationContext(), FallDetectionService.class);
        getApplicationContext().startService(service);
        JobServiceHandler.scheduleJob(getApplicationContext()); //reschedule the job
        return true;
    }
    @Override
    public boolean onStopJob(JobParameters params) {
        return true;
    }

}
