package com.mobile.freefalldetection;

interface FallDetectionListener {

    void onFallEnd(Long startTimeStamp,Long duration);

}
