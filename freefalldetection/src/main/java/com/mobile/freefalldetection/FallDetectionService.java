package com.mobile.freefalldetection;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;

public class FallDetectionService extends Service implements SensorEventListener,FallDetectionListener {

    private SensorManager senSensorManager;
    private FallDetectionWrapper fallDetectionWrapper;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        sensorInitialize();
        detectionInitialize();

        return super.onStartCommand(intent, flags, startId);

    }

    /*
    * sensors must register before usage. for more information about different types of Sensors, or different types of Accelerometer
    * please visit this link: https://developer.android.com/guide/topics/sensors/sensors_overview
     */

    private void sensorInitialize(){
        senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        assert senSensorManager != null;
        Sensor senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    private void detectionInitialize(){
        fallDetectionWrapper = new FallDetectionWrapper();
        fallDetectionWrapper.setFallDetectionListener(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        fallDetectionWrapper.setEvent(event);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onFallEnd(Long startTimeStamp, Long duration) {

        String formattedDuration = Utility.getTime(duration);
        String formattedStartTimeStamp = Utility.getDate(startTimeStamp);

        Notification.GeneratePushNotification(getBaseContext(),formattedDuration);
        databaseInsertion(formattedStartTimeStamp,formattedDuration);

    }

    private void databaseInsertion(String startTimeStamp,String formattedDuration){
        EventDataBaseWrapper eventDataBaseWrapper = new EventDataBaseWrapper(getBaseContext());
        eventDataBaseWrapper.open();
        eventDataBaseWrapper.insert(startTimeStamp,formattedDuration);
        eventDataBaseWrapper.close();
    }

    @Override
    public void onDestroy() {
        senSensorManager.unregisterListener(this);
        restartService();
        super.onDestroy();
    }

    private void restartService(){
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("ServiceRestart");
        broadcastIntent.setClass(this, ServiceRestart.class);
        this.sendBroadcast(broadcastIntent);
    }

}
