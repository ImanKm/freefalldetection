package com.mobile.freefalldetection;

import android.hardware.SensorEvent;

class FallDetectionWrapper {

    private Long startTimeStamp = 0L;
    private boolean isInFalling = false;
    private FallDetectionListener fallDetectionListener;


    void setFallDetectionListener(FallDetectionListener fallDetectionListener) {
        this.fallDetectionListener = fallDetectionListener;
    }

    //calculating event and reserve start time. if the free fall happens, the related listener must be triggered.
    void setEvent(SensorEvent event) {

        boolean flag = AccelerateCalculation.detect(event.values);

        if( flag && !isInFalling ){
            activeInFalling();
        }else if(isInFalling && !flag){
            deActiveInFalling();
        }

    }

    private void activeInFalling(){
        isInFalling = true;
        startTimeStamp = System.currentTimeMillis();
    }

    private void deActiveInFalling(){
        isInFalling = false;
        fallDetectionListener.onFallEnd(startTimeStamp,System.currentTimeMillis() - startTimeStamp);
    }



}
