package com.mobile.freefalldetection;

public class FallModel {

    private String startTime;
    private String durationTime;

    public String getStartTime() {
        return startTime;
    }

    void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getDurationTime() {
        return durationTime;
    }

    void setDurationTime(String durationTime) {
        this.durationTime = durationTime;
    }

}
