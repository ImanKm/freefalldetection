package com.mobile.freefalldetection;

import android.app.Activity;
import android.content.Context;

public class FreeFallDetection {

    private Context context;

    public FreeFallDetection(Activity activity) {
        this.context =  activity.getBaseContext();
        addClassName(activity);
    }

    //This name must save to find the target class when user
    // want to redirect to the activity of app, after click on notification.
    private void addClassName(Activity activity){
        PreferenceManager.write(activity.getBaseContext(), activity.getComponentName().getClassName());
    }

   public void run(){
       ServiceHandler.run(context);
   }

}
