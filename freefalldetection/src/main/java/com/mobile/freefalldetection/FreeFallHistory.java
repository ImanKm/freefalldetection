package com.mobile.freefalldetection;

import android.content.Context;
import android.database.Cursor;
import java.util.ArrayList;

import static com.mobile.freefalldetection.EventDataBaseWrapper.COL_Fall_Duration;
import static com.mobile.freefalldetection.EventDataBaseWrapper.COL_Fall_Start_TimeStamp;

public class FreeFallHistory {

    public ArrayList<FallModel> getEventList(Context context){

        EventDataBaseWrapper eventDataBaseWrapper = new EventDataBaseWrapper(context);
        eventDataBaseWrapper.open();

        Cursor cursor = eventDataBaseWrapper.fetchAll();

        ArrayList<FallModel> arrayLists = new ArrayList<>();

        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            FallModel fallModel = new FallModel();
            fallModel.setStartTime(cursor.getString(cursor.getColumnIndex(COL_Fall_Start_TimeStamp)));
            fallModel.setDurationTime(cursor.getString(cursor.getColumnIndex(COL_Fall_Duration)));
            arrayLists.add(fallModel);
            cursor.moveToNext();
        }

        cursor.close();
        eventDataBaseWrapper.close();

        return arrayLists ;
    }



}
