package com.mobile.freefalldetection;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import androidx.annotation.RequiresApi;

class JobServiceHandler {
    // schedule the start of the service every 1 to 3 seconds
    @RequiresApi(api = Build.VERSION_CODES.M)
     static void scheduleJob(Context context) {
        ComponentName serviceComponent = new ComponentName(context, FallDetectionJobService.class);
        JobInfo.Builder builder = new JobInfo.Builder(0, serviceComponent);
        builder.setMinimumLatency(1000); // wait at least
        builder.setOverrideDeadline(3 * 1000); // maximum delay
        JobScheduler jobScheduler  = context.getSystemService(JobScheduler.class);

        assert jobScheduler != null;
        jobScheduler.schedule(builder.build());

    }

}