package com.mobile.freefalldetection;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import androidx.core.app.NotificationCompat;

class Notification {


      static void   GeneratePushNotification(Context context, String message) {

        Drawable drawable = null;

        try {
            drawable = context.getPackageManager().getApplicationIcon("com.mobile.falldetection");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

          assert drawable != null;
          NotificationCompat.Builder mBuilder =   new NotificationCompat.Builder(context,"")
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setLargeIcon(((BitmapDrawable)drawable).getBitmap())
                .setContentTitle(context.getResources().getString(R.string.free_fall_detection))
                .setContentText(context.getResources().getString(R.string.detection)+message)
                .setAutoCancel(true);

        Intent intent = new Intent();
        try {
            intent = new Intent(context,Class.forName(PreferenceManager.read(context)));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        
        PendingIntent pi = PendingIntent.getActivity(context,0,intent,0);
        
        mBuilder.setContentIntent(pi);
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        assert mNotificationManager != null;
        mNotificationManager.notify(0, mBuilder.build());

      }

}
