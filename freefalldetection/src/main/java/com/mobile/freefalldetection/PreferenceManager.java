package com.mobile.freefalldetection;

import android.content.Context;
import android.content.SharedPreferences;

 class PreferenceManager {

    private static final String TAG = "tag";
    private static final String IntentClassName = "IntentClassName";
    private static SharedPreferences sharedPreferences;


    //Read from sharedPrefrence
      static String read(Context context) {
         sharedPreferences = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
         return sharedPreferences.getString(PreferenceManager.IntentClassName, null);
     }


     //write to sharedPrefrence
      static void write(Context context, String value) {
         sharedPreferences = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
         sharedPreferences.edit().putString(PreferenceManager.IntentClassName, value).apply();
     }

}
