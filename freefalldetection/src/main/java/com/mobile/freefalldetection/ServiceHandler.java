package com.mobile.freefalldetection;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.RequiresApi;

public class ServiceHandler {


    //Making desicion to call proper service by checking the version of the OS of the device
    //Note: Orio+ needs JobService
    public static void run(Context context){
        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            runScheduleJob(context);
        }else{
            runSimpleService(context);
        }
    }

    @SuppressLint("WrongConstant")
    private static void runSimpleService(Context context){
        Intent i= new Intent(context, FallDetectionService.class);
        i.addFlags(Service.START_STICKY);//create again on low memory with no calling previous intent
        context.startService(i);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private static void runScheduleJob(Context context) {
        JobServiceHandler.scheduleJob(context);
    }

}
