package com.mobile.freefalldetection;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


public class ServiceRestart extends BroadcastReceiver {

    //Call service again after its destroy
    @Override
    public void onReceive(Context context, Intent intent) {
        ServiceHandler.run(context);
    }
}