package com.mobile.freefalldetection;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

class Utility {

    private static DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss", Locale.getDefault());

    static String getTime(Long duration) {

        if (duration == null) {
            return "";
        }

        long totalSeconds = (duration / 1000);
        long miliSeconds = (duration - (totalSeconds * 1000));
        long minutes = totalSeconds / 60;
        long hours = (minutes / 60) % 24;
        int days = (int) (minutes / (60 * 24));
        return "" + days + "d " + makeFormat(hours) + ":" + makeFormat(minutes % 60) + ":" + makeFormat(totalSeconds % 60) + ":" + makeFormat(miliSeconds);
    }

    static String getDate(Long timestamp) {

        if (timestamp == null) {
            return "";
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        return formatter.format(calendar.getTime());
    }

    private static String makeFormat(long number) {
        if (number < 10) {
            return "0" + number;
        } else {
            return "" + number;
        }
    }

}
