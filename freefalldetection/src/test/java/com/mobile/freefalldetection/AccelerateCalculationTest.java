package com.mobile.freefalldetection;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Arrays;
import java.util.Collection;


@RunWith(Parameterized.class)
public class AccelerateCalculationTest {

    private float[] input;
    private boolean expected;

    public AccelerateCalculationTest(float[] input, boolean expected) {
        this.expected = expected;
        this.input = input;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> parameters() {
        Object[][] data = new Object[][]{

                { new float[]{},false},
                { null,false},
                { new float[]{0,0,0},false},
                { new float[]{0,0,9.8f},false},
                { new float[]{0,9.8f,0},false},
                { new float[]{9.8f,0,0},false},
                { new float[]{9.7f,0,0},false},
                { new float[]{10,0,0},false},
                { new float[]{9.8f,9.8f,0},false},
                { new float[]{1,1,0},false},

        };
        return Arrays.asList(data);
    }


    @Test
    public void executeParameterizedTest() throws Exception {
        Assert.assertEquals(expected, AccelerateCalculation.detect(input));

    }


}
