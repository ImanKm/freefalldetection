package com.mobile.freefalldetection;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Arrays;
import java.util.Collection;


@RunWith(Parameterized.class)
public class UtilityGetDateTest {

    private Long input;
    private String expected;

    public UtilityGetDateTest(Long input, String expected) {
        this.expected = expected;
        this.input = input;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> parameters() {
        Object[][] data = new Object[][]{

                        { 0000000000000L,"01/01/1970 01:00:00"},
                        { 9999999999999L,"20/11/2286 06:46:39"},
                        { null,""},
                        { 1581790126171L,"15/02/2020 07:08:46"},

        };
        return Arrays.asList(data);
    }


    @Test
    public void executeParameterizedTest() throws Exception {

            Assert.assertEquals(expected, Utility.getDate(input));

    }


}
