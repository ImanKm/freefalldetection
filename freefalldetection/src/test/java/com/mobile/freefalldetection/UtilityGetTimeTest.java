package com.mobile.freefalldetection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Arrays;
import java.util.Collection;



@RunWith(Parameterized.class)
public class UtilityGetTimeTest {

    private Long input;
    private String expected;

    public UtilityGetTimeTest(Long input, String expected) {
        this.expected = expected;
        this.input = input;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> parameters() {
        Object[][] data = new Object[][]{
                { 0000000000000L,"0d 00:00:00:00"},
                { 9999999999999L,"115740d 17:46:39:999"},
                { null,""},
                { 1581790126171L,"18307d 18:08:46:171"},
                { 0000000000001L,"0d 00:00:00:01"}
        };
        return Arrays.asList(data);
    }

    @Test
    public void executeParameterizedTest() throws Exception {
            Assert.assertEquals(expected, Utility.getTime(input));
    }

}
